class AddColumnToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :submitter_id, :integer
  end
end
