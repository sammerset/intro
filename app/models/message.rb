require 'csv'

class Message < ActiveRecord::Base
  validates :name, :email, :subject, :content, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, 
                    allow_blank: true
                    
  belongs_to :submitter
  
  class << self
    def import!
      Thread.new do
        CSV.foreach(import_file, :headers => true) do |row|
          create(row.to_hash) do |m|
            m.submitter = Submitter.where(email: m.email).first_or_create
          end
        end
        ActiveRecord::Base.connection.close
      end
    end
    
    def add_to_csv(object)
      return false if object.invalid?
      CSV.open(import_file, "a+") do |csv|
        csv << object.attributes.values_at(*column_names)
      end
    end
  
    def import_file
      CSV.open(file_path, "a+"){|csv| csv << column_names} unless File.exist?(file_path)
      file_path
    end
    
    def file_path
      Rails.application.config.csv_file
    end
  end
end
