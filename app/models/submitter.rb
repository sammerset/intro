class Submitter < ActiveRecord::Base
  validates :name, :email, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i },
                    uniqueness: true, 
                    allow_blank: true
  has_many :messages
end
