class MessagesController < ApplicationController
  def index
    @messages = Message.all
  end

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)
    respond_to do |format|
      if Message.add_to_csv(@message)
        format.html { redirect_to root_url, notice: 'Message was successfully sent!' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    @message = Message.find(params[:id])
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
    end
  end

  def export_to_db
    Message.import!
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Messages importing in process. Please refresh page in few minutes.' }
    end
  end
  
  protected
  
  def message_params
    params.require(:message).permit(:name, :email, :subject, :content)
  end 
end
